import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class StringCalculatorTest {

    private StringCalculator instanceToTest = new StringCalculator();

    @Test
    public void whenEmptyString_shouldReturnZero(){
        String input = "";
        int expected = 0;
        int actual = instanceToTest.add(input);

        assertEquals(expected, actual);
    }

    @Test
    public void whenStringNumberIsThree_shouldReturnIntNumberThree(){
        String input = "3";
        int expected = 3;

        int actual = instanceToTest.add(input);

        assertEquals(expected,actual);

    }

    @Test
    public void whenStringNumbersAreThreeAndFour_shouldReturnIntNumbersThreeAndFour() {
        String input = "3,4";
        int expected = 7;
        int actual = instanceToTest.add(input);

        assertEquals(expected,actual);
    }

    @Test
    public void whenStringLengthInputIsUnknown_shouldReturnSumOfAll(){
        String input = "1,2,3,4";
        int expected = 10;
        int actual = instanceToTest.add(input);

        assertEquals(expected,actual);

    }

    @Test
    public void whenInputHasNewline_shouldReturnSumOfAll() {
        String input = "2\n3,4";
        int expected = 9;
        int actual = instanceToTest.add(input);

        assertEquals(expected, actual);
    }

    @Test
    public void whenInputHasAnyDelimiter_shouldRetunrSumOfAll(){
        String input = "//;\n1;2";
        int expected = 3;
        int actual = instanceToTest.add(input);

        assertEquals(expected, actual);
    }
}

public class StringCalculator {


    public int add(String input) {
        if(input == "") return 0;

        String[] inputs = input.split("[\\W]");
        int sum = 0;
        for(int i=0; i < inputs.length; i++) {
            sum += Integer.parseInt(inputs[i]);
        }

        return sum;
    }
}
